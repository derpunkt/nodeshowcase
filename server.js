try {
    require('./local/agent');
} catch (err) {
    console.error("Please create a file local/agent.js with the agent code in it. Look at local/agent.js.sample");
}


console.log("I'm starting");

// Require libraries
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var lessMiddleware = require('less-middleware');
var bodyParser = require('body-parser');

var index = require('./app/routes');
var weather = require('./app/routes/weather');
var problem = require('./app/routes/problem');
var todo = require('./app/routes/todo');
var favicon = require('serve-favicon');
var cluster = require('cluster');

// Define some error handlers
var onError = function (error) {
    console.log('onError: ');
    console.log(error);
    process.exit(1);
};

var onUncaughtException = function (ex) {
    console.log('onUncaughtException: ');
    console.log(ex);
    console.log(ex.stack);
    process.exit(1);
};

cluster.on('exit', function (worker) {

    // Replace the dead worker,
    // we're not sentimental
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();

});


if (cluster.isMaster) {

    console.log("I'm a master!");

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

// Code to run if we're in a worker process
} else {

    console.log("I'm a child!");

    // Configure app and middlewares
    var app = express();
    app.locals.pretty = true;
    app.use(favicon(__dirname + '/public/favicon.ico'));
    app.set('view engine', 'jade');
    app.set('views', __dirname + '/app/views');
    app.use(lessMiddleware(__dirname + '/public'));
    app.use(express.static(__dirname + '/public'));
    app.use(bodyParser.json());

    app.use('/', index);
    app.use('/weather', weather);
    app.use('/todo', todo);
    app.use('/problem', problem);


    // Setup a database connection
    var url = 'mongodb://localhost:27017/showcase';
    MongoClient.connect(url, function (err, db) {
        if (err) throw err;

        console.log("Connected to mongodb.");
        app.db = db;

        // Put the web request listener on the event loop
        var server = app.listen(3000, function () {
            var port = server.address().port;
            console.log('Express running on %s', port);
        });
        // HTTP Server level error like EADDRINUSE
        server.on('error', onError);
        // Uncaught exceptions
        server.on('uncaughtException', onUncaughtException);
    });

}




