var express = require('express');
var router = express.Router();
var sa = require('superagent');

router.get('/', function(req, res, next) {

    sa.get('https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=dynatrace')
        .accept('json')
        .end(function(e, r) {
            if(e) return next(e);

            res.render('index', {
                news: JSON.parse(r.text)
            });

        });


});

module.exports = router;