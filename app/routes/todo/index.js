var express = require('express');
var router = express.Router();

router.post('/', function (req, res, next) {

    if (!req.body.todoItem) return res.status(200).end();

    var db = req.app.db;
    var collection = db.collection('todos');

    collection.insert({
        text: req.body.todoItem
    }, function (e, r) {
        if (e) return next(e);

        console.log("Insert a todo item");
        collection.find({}).toArray(function (e, r) {
            if (e) return next(e);
            return res.json(r);
        });
    });

});


router.get('/', function (req, res, next) {
    var db = req.app.db;
    var collection = db.collection('todos');
    collection.find({}).toArray(function (e, r) {

        if (e) return next(e);
        return res.json(r);
    });
});


module.exports = router;