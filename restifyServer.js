try {
    require('./local/agent');
} catch (err) {
    console.error("Please create a file local/agent.js with the agent code in it. Look at local/agent.js.sample");
}

var restify = require('restify');
var sa = require('superagent');
var OpenWeatherMapService = require('./app/services/OpenWeatherMap');
var owm = new OpenWeatherMapService();

function respond(req, res, next) {

    var lat = 48.3;
    var lng = 14.2833;

    owm.fetchLocation(lat, lng, function(e, r) {
        if(e) {
            return next(e);
        }

        res.send(r);
        next();
    });


}

function respondCoordinates(req, res, next) {

    var lat = req.params.lat;
    var lng = req.params.lng;

    owm.fetchLocation(lat, lng, function(e, r) {
        if(e) {
            return next(e);
        }

	console.log(r);
        res.json(r);
        next();
    });


}



var server = restify.createServer();
server.get('/', respond);

server.get('/:lat/:lng', respondCoordinates);



server.listen(3000, function() {
    console.log('%s listening at %s', server.name, server.url);
});
