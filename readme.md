## Installation

### Prerequisites
* git is installed
* Node.js 0.10.x and npm are installed
* MongoDB is installed

### Prepare dtServer
Up until around Dynatrace 6.3 the server needs a flag to be set to open the http port:
This is done by inserting `-Dcom.dynatrace.diagnostics.startDtangAdapter=true` into `dtserver.ini`


### Checkout and module installation
```sh
    $ git clone https://derpunkt@bitbucket.org/derpunkt/nodeshowcase.git
    $ cd nodeshowcase
    $ npm install
```

### Agent setup
```sh
    $ cd local
    $ cp agent.js.local agent.js

    # Edit agent path, name and server address (if required)
    $ nano | vi | whatever agent.js
```

## Running the example apps

### Express.js

```sh
    $ nodejs server.js
```

* The app should be accessible at http://<server_address>:3000 and show a website.
* PurePath and transaction flow should show outgoing http requests to openweathermap.org and ajax.googleapis.com

### Restify.js

```sh
    $ nodejs restifyServer.js
```

* The app should be accessible at http://<server_address>:3000 and should return a json with weather data
* PurePath and transaction flow should show outgoing http requests to openweathermap.org