// Dynatrace Agent initialisation
try {
    require('/opt/dynatrace-6.2//agent/conf/nodejsagent.js')({
        server: 'http://localhost:8020', tenant: '1'
    });
} catch (err) {
    console.error(err.toString());
}

var Hapi = require('hapi');
var OpenWeatherMapService = require('./app/services/OpenWeatherMap');
var owm = new OpenWeatherMapService();

// Create a server with a host and port
var server = new Hapi.Server();
server.connection({
    // host: 'localhost',
    port: 3000
});

// Add the route
server.route({
    method: 'GET',
    path: '/hello',
    handler: function (request, reply) {
        owm.fetchLocation(52.51, 13.76, function (e, r) {
            if (e) {
                console.log("Error: " + e);
            }
            reply(r);
        });
    }
});

// Start the server
server.start();

